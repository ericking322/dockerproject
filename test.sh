#!/bin/bash

#HTTP Connection
if wget --spider http://localhost:80 2>/dev/null
then
    echo "True"
else
    echo "False"
fi


#HTTPS Connection
if wget --spider https://localhost:433 2>/dev/null
then
    echo "True"
else
    echo "False"
fi

#HTTPS Connection on Port 80 and force not to redirect
curl -k https://localhost:80
