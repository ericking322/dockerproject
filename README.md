Docker App
***

Docker compose contains image of the lastest version of Apache from  [Bitnami](https://bitnami.com/) 

## Installation
Install docker on local machine then navigate to folder containing file docker-compose.
modify volumes to match your local file path.
Run command 'docker-compose up'
Docker will download Apache webserver and load our settings from paths labeled under volumes. Please modify paths based on your OS, Directory structure and also read/write permission levels.

## Usage
We'll make script executable with chmod +x command. To find the script you have to type the script path for the shell.



## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[Ekinde](ekinde14@gmail.com)

